import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConnectComponent }   from './connect/connect.component';
import { EspacePersoComponent }      from './espace-perso/espace-perso.component';

import { RecipeComponent }      from './recipe/recipe.component';
import { AddRecipeComponent }      from './add-recipe/add-recipe.component';
import { ManageComponent }  from './manage/manage.component';
import { StockComponent }  from './stock/stock.component';
import { SuggestionComponent }  from './suggestion/suggestion.component';

import { RechercheComponent }      from './recherche/recherche.component';
import { ResultatsComponent }  from './resultats/resultats.component';


const routes: Routes = [
  { path: '', redirectTo: '/connect', pathMatch: 'full' },
  { path: 'connect', component: ConnectComponent },
  { path: 'espace-perso/:id', component: EspacePersoComponent },

  { path: 'recipe/:id', component: RecipeComponent },
  { path: 'add-recipe', component: AddRecipeComponent },
  { path: 'manage', component: ManageComponent },
  { path: 'stock', component: StockComponent },
  { path: 'suggestion', component: SuggestionComponent },

  { path: 'recherche', component: RechercheComponent },
  { path: 'resultats', component: ResultatsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
