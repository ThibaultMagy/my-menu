export class User {
  id: number;
  name: string;
  img: string;
  prefRecipes: string[];
}
