import { User } from './user';

export const USERS: User[] = [
  { id: 1, name: 'thibault', img: "assets/oss.jpg", prefRecipes: [] },
  { id: 2, name: 'tifusaurus', img: "assets/tifu.jpg", prefRecipes: [] },
  { id: 3, name: 'lieutenantMaster', img: "assets/lieutenant.jpg", prefRecipes: [] },
];
