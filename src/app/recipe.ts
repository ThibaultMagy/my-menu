export class Recipe {
    id: number;
    name: string;
    pic: string;
    time: string;
    consumers: string;
    ingredients: [];
    protocol: string;
  }