import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Recipe } from './recipe';


@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const recipes = [
      { id: 11, name: "Poulet Bicyclette", img:"assets/pouletBicyclette.jpg",
      desc:" Un poulet traditionnel sort du poulailler le matin tôt, court toute la journée comme une bicyclette à la recherche de sa nourriture et au crépuscule il retourne dans son poulailler.",
      time:"50min",consumers:"4",ingredient:[
        "1 poulet",
        "2 oignons",
        "3 gousses d'ail",
        "1/2 cuillère à café de poudre de gingembre",
        "2 feuilles de laurier",
        "légumes (carottes, navets, ...)",
        "2 pommes de terre",
        "une noix de beurre",
        "2 cuillères à soupe de vinaigre",
        "Sel, poivre"
      ],
      protocol:"« Un poulet traditionnel sort du poulailler le matin tôt, court toute la journée comme une bicyclette à la recherche de sa nourriture et au crépuscule il retourne dans son poulailler ». Au Sénégal, il est très courant de voir dans les petits villages des poulets aller librement de cour en cour. Certains les appellent « poulet bicyclette », en raison de leur démarche rapide et saccadée, tandis que d’autres les nomment simplement « poulet local » ou « poulet de brousse ».Il présente de nombreuses différences par rapport au poulet d’élevage intensif, mais ce qui rend le poulet bicyclette si apprécié est surtout sa viande, plus goûteuse et savoureuse.<br/>Nettoyer et couper le poulet en gros morceaux. Cuire le poulet au pot-au feu, dans une marmite ou une cocotte-minute avec le laurier, les gousses d’ail, la poudre de gingembre et du sel.Une fois les morceaux de poulet cuits et tendres, les mettre à mariner avec le vinaigre, rectifier en sel, les faire revenir dans l’huile chaude jusqu’à ce que chaque face soit dorée. Pour une cuisson plus légère, les mettre au four en allumant le grill.Cuire les légumes et les pommes de terre à la vapeur. Couper le reste de l’oignon en petit dés. Avec le morceau de beurre, faire sauter les oignons et les légumes.Saler, poivrer et servir le poulet accompagné du sauté de légumes et de pommes de terre."}
    ];
    return {recipes};
  }

  // Overrides the genId method to ensure that a recipe always has an id.
  // If the recipes array is empty,
  // the method below returns the initial number (11).
  // if the recipes array is not empty, the method below returns the highest
  // recipe id + 1.
  genId(recipes: Recipe[]): number {
    return recipes.length > 0 ? Math.max(...recipes.map(recipe => recipe.id)) + 1 : 11;
  }
}
