import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { RECIPES } from '../mock-recipes';
import { RecipeService } from '../recipe.service';

import { SuggestionComponent } from './suggestion.component';
import { RechercheComponent } from '../recherche/recherche.component';

describe('SuggestionComponent', () => {
  let component: SuggestionComponent;
  let fixture: ComponentFixture<SuggestionComponent>;
  let recipeService;
  let getRecipesSpy;

  beforeEach(async(() => {
    recipeService = jasmine.createSpyObj('RecipeService', ['getRecipes']);
    getRecipesSpy = recipeService.getRecipes.and.returnValue( of(RECIPES) );
    TestBed.configureTestingModule({
      declarations: [
        SuggestionComponent,
        RechercheComponent
      ],
      imports: [
        RouterTestingModule.withRoutes([])
      ],
      providers: [
        { provide: RecipeService, useValue: recipeService }
      ]
    })
    .compileComponents();

  }));

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuggestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuggestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display "Five recipes" as headline', () => {
    expect(fixture.nativeElement.querySelector('h3').textContent).toEqual('Five recipes');
  });

  it('should call jobService', async(() => {
    expect(getRecipesSpy.calls.any()).toBe(true);
    }));

  it('should display 4 links', async(() => {
    expect(fixture.nativeElement.querySelectorAll('a').length).toEqual(4);
  }));
});
